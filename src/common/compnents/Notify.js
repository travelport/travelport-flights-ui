import React from 'react';

const Notify = (props) => (
	<div class="notification is-danger">
		<button
			class="delete"
			onClick={() => {
				props.handleClick(false);
			}}
		/>
		{props.error}
	</div>
);

export default Notify;
