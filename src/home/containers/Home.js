import React, { Component } from 'react';
import { Route, Redirect } from 'react-router';
import Intro from '../components/Intro';

class Home extends Component {
	state = {
		toSearch: false
	};

	handleClick = (e) => {
		e.preventDefault();
		this.setState(() => ({
			toSearch: true
		}));
	};

	render() {
		if (this.state.toSearch === true) {
			return <Redirect to="/search" />;
		}

		return (
			<section className="section section-feature-grey is-medium">
				<div cclassNamelass="hero-body">
					<div className="container">
						<Intro handleClick={(e) => this.handleClick(e)} />
					</div>
				</div>
			</section>
		);
	}
}

export default Home;
