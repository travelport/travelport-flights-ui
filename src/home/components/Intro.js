import React, { Component } from 'react';

const Intro = (props) => (
	<div className="columns is-vcentered">
		<div className="column is-5 is-offset-1 landing-caption">
			<h1 className="title is-1 is-bold is-spaced">We'll help you find the perfect flight.</h1>
			<h2 className="subtitle is-5 is-muted">
			It is one click away for your dream journey !!!
			</h2>
			<p>
				<a
					className="button cta rounded primary-btn raised"
					onClick={(e) => {
						props.handleClick(e);
					}}
				>
					Get Started
				</a>
			</p>
		</div>
		<div className="column is-5 is-offset-1">
			<figure className="image is-4by3">
				
			</figure>
		</div>
	</div>
);


export default Intro;