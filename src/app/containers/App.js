import React from 'react';
import Layout from '../components/Layout';
import ApplicationRoutes from '../route/ApplicationRoutes'

class App extends React.Component {
	render() {
		return (
			<Layout>
				<ApplicationRoutes />
			</Layout>
		);
	}
}

export default App;
