import React from 'react';

import styled from 'styled-components';

// const BrandedButton = styled.button`
//   color: ${props => props.themeColor};
//   &:hover {
//     color: ${props => props.themeHoverColor};
//   }
// `
// render(){
//   return (
//     <BrandedButton themeHoverColor="pink" themeColor="blue" >
//       Click Me!
//     </BrandedButton>
//   );
// }

// const Button = styled.button`
//   color: ${props => props.isSecondary ? ‘blue’ : ‘white’};
// `

const Footer = () => (
	<footer className="footer footer-dark">
		<div className="container">
			<div className="columns">
				<div className="column">
					<div className="footer-logo">
						
					</div>
				</div>
				<div className="column">
					<div className="footer-column" />
				</div>
			</div>
		</div>
	</footer>
);

export default Footer;
