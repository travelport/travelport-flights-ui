import React from 'react';
import Logo from './Logo';
import styled, { keyframes } from 'styled-components';




const rotate360 = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`

const AppLogo = styled.img`
  animation: ${rotate360} infinite 120s linear;
  height: 80px;
  &:hover {
    animation: ${rotate360} infinite 1.5s linear;
  }
`

class Header extends React.Component {
	state = {
		isActive: false
	};

	toggleNav = () => {
		this.setState((prevState) => ({
			isActive: !prevState.isActive
		}));
	};

	render() {
		return (
			<nav className="navbar is-fresh is-transparent no-shadow" role="navigation" aria-label="main navigation">
				<div className="container">
					<div className="navbar-brand">
						<a className="navbar-item" href="#">
							<Logo />
						</a>
						<a
							role="button"
							className="navbar-burger"
							aria-label="menu"
							aria-expanded="false"
							data-target="navbar-menu"
						>
							<span aria-hidden="true" />
							<span aria-hidden="true" />
							<span aria-hidden="true" />
						</a>
					</div>

					<div id="navbar-menu" className="navbar-menu is-static">
						<div className="navbar-end">
							<a href="#" className="navbar-item is-secondary">
								Home
							</a>
							<a href="#" className="navbar-item is-secondary">
								Help
							</a>
							<div className="navbar-item has-dropdown is-hoverable">
								<a className="navbar-link">Language</a>

								<div className="navbar-dropdown">
									<a className="navbar-item">English</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</nav>
		);
	}
}

export default Header;
