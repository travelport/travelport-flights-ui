import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import Header from './Header';
import Content from './Content';
import Footer from './Footer';

const Layout = ({ children }) => (
	<div className="container is-fluid">
		<Helmet
			title="Travelport - Flights"
			meta={[
				{ name: 'description', content: 'Quick Flight Search for destination' },
				{ name: 'keywords', content: 'Flights, Travel, booking, Trip ' }
			]}
			script={[ { src: 'https://use.fontawesome.com/releases/v5.1.0/js/all.js' } ]}
			link={[
				{
					rel: 'stylesheet',
					href: 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'
				}
			]}
		/>
		<Header />
		<Content>{children}</Content>
		<Footer />
	</div>
);

Layout.propTypes = {
	children: PropTypes.func
};

export default Layout;
