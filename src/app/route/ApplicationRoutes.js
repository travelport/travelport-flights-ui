import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';
import createHistory from 'history/createBrowserHistory';
import Loadable from 'react-loadable';
import ApplicationNavigationConfig from '../config/ApplicationNavigationConfig';
import { Home } from '../../home';
import { Search } from '../../search';

const Loading = () => <div>Loading...</div>;

const LoadRoutes = () => {
	return ApplicationNavigationConfig.map((route) => ({
		path: route.path,
		component: Loadable({
			loader: () => import('../../home'),
			loading: Loading
		})
	}));
};

class ApplicationRoutes extends Component {
	render() {
		const history = createHistory();
		//const routes = LoadRoutes();
		return (
			<Router history={history}>
				<Switch>
					<Route exact path="/" component={Home} />
					<Route path="/search" component={Search} />
				</Switch>
			</Router>
		);
	}
}

export default ApplicationRoutes;
