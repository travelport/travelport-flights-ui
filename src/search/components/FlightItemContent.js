import React from 'react';
import FlightTimeItem from './FlightTimeItem';
import FlightItem from './FlightItem';
import FlightPriceItem from './FlightPriceItem';

const FlightItemContent = (props) => {
	const { depature,airline, arrival, deaptureTime, price, duration,arrivalTime } = props.flight;

	return (
		<FlightItem icon="fas fa-plane" title={airline}>
			<div class="columns">
				<div class="column is-three-quarters">
					<article>
						<div class="level">
							<FlightTimeItem
								icon="fas fa-plane-departure"
								type="Depature"
								port={depature}
								time={deaptureTime}
							/>
						</div>
						<div class="is-divider" data-content="" />
						<div class="level">
							<FlightTimeItem
								icon="fas fa-plane-arrival"
								type="Arrival"
								port={arrival}
								time={arrivalTime}
							/>
						</div>
					</article>
				</div>
				<div className="is-divider-vertical" data-content="BOOK" />
				<div class="column">
					<FlightPriceItem price={price} duration={duration} />
				</div>
			</div>
		</FlightItem>
	);
};

export default FlightItemContent;

// key: flight.key,
// 					airline: `${flight.airline.name}/${flight.airline.code}/${flight.plane
// 						.shortName}/${flight.flightNum}`,
// 					depature: flight.start.name,
// 					deaptureTime: flight.start.dateTime,
// 					arrival: flight.finish.name,
// 					arrivalTime: flight.finish.dateTime,
// 					duration: flight.durationMin,
// 					price: flight.price
