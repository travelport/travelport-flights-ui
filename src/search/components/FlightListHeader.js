import React from 'react';
import FlightListItem from './FlightListItem';

const FlightListHeader = (props) => {
	const { departureAirport, destinationAirport, flyingDate, returnDate } = props.filters;
	return (
		<div class="level">
			<FlightListItem title="Departure Airport" description={departureAirport} />
            <FlightListItem title="Destination Airport" description={destinationAirport} />
            <FlightListItem title="Flying Date" description={flyingDate} />
            {returnDate && <FlightListItem title="Return Date" description={returnDate} />}
		</div>
	);
};

export default FlightListHeader;
