import React from 'react';
import FlightItem from './FlightItem';
import FlightItemContent from './FlightItemContent';
import FlightPriceItem from './FlightPriceItem';

const FlightItemContents = (props) => {
	const { departureFlights, returnFlights } = props.flight;

	return props.tabKey == 'G' ? (
		<article>
			{departureFlights.map((flights, index) =>
				flights.map((flight) => <FlightItemContent key={index} flight={flight} />)
			)}
		</article>
	) : (
		<article>
			{returnFlights.map((flights, index) =>
				flights.map((flight) => <FlightItemContent key={index} flight={flight} />)
			)}
		</article>
	);
};

export default FlightItemContents;
