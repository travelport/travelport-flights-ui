import React from 'react';

const FlightPriceItem = (props) => {
	return <div class="level-item ">
		<div className=" pad-top-med">
			<p className="heading has-text-danger">{props.duration}.min</p>
            <p className="title is-4">${props.price}</p>
			<div class="control">
				<a
					class="button is-info is-medium"
					onClick={(e) => {
						console.log("booking sent")
					}}
				>
					<span>Book</span>
				</a>
			</div>
		</div>
	</div>
};

export default FlightPriceItem;
