import React, { Component } from 'react';
import moment from 'moment';
import FlightAutoSuggesstion from '../components/FlightAutoSuggesstion';
import FlightDayPickerInput from '../components/FlightDayPickerInput';
import FlightCheckInput from '../components/FlightCheckInput';
import FlightItem from './FlightItem';
import SearchFilterService from '../services/SearchFilterService';
import {Notify} from '../../common';

const searchFilterService = new SearchFilterService();

class FlightFilter extends React.Component {
	constructor() {
		super();
		this.handleDayChange = this.handleDayChange.bind(this);
		this.handleFlyightType = this.handleFlyightType.bind(this);
		this.handleSearch = this.handleSearch.bind(this);

		this.state = {
			depature: '',
			arrival: '',
			flyingDay: '',
			returnDay: '',
			isReturn: true,
			inValid: false,
			error: 'Please fill your journey details, its help you to get good results',
			isLoading: false
		};
	}

	getSuggestions = async (value) => {
		const inputValue = value.trim().toLowerCase();
		return inputValue.length >= 3 ? await searchFilterService.getFlights(inputValue) : [];
	};

	getSelected(value, isDepature) {
		if (isDepature) {
			this.setState({ depature: value });
		} else {
			this.setState({ arrival: value });
		}
	}

	handleDayChange(day, flag) {
		if (flag) {
			this.setState({ flyingDay: day });
		} else {
			this.setState({ returnDay: day });
		}
	}

	handleFlyightType(flag) {
		this.setState({ isReturn: flag });
	}

	handleSearch(e) {
		let { depature, arrival, flyingDay, returnDay, isReturn } = this.state;

		if (depature && arrival && flyingDay) {
			if (isReturn && !returnDay) {
				this.setState({
					inValid: true,
					error: 'Please fill your journey details, its help you to get good results'
				});
			} else {
				if (isReturn && moment(flyingDay).isAfter(returnDay)) {
					this.setState({
						inValid: true,
						error: 'Please make flying Day after your return Day'
					});
				} else {
					this.setState({ isLoading: true });
					this.props.handleSearch({
						departureAirport: depature,
						destinationAirport: arrival,
						flyingDate: moment(flyingDay).format('YYYY-MM-DD'),
						returnDate: returnDay && moment(returnDay).format('YYYY-MM-DD'),
						isReturn: isReturn
					});
				}
			}
		} else {
			this.setState({ inValid: true });
		}
	}

	render() {
		return (
			<section className="section section-feature-grey is-medium">
				<div className="container is-loading">
					<div class="columns is-centered">
						<div class="column is-three-fifths is-narrow">
							{this.state.inValid && (
								<Notify error={this.state.error} handleClick={(flag)=> this.setState({ inValid: flag })}/>
							)}
							<FlightItem
								icon="fas fa-map-marker-alt"
								title="Where would you like to Fly to enjoy your journey"
								type="?"
							>
								<div class="columns is-multiline">
									<div class="column is-half is-narrow">
										<FlightAutoSuggesstion
											title="Departure Airport"
											icon="fas fa-plane-departure"
											getSuggestions={this.getSuggestions}
											getSelected={(val) => this.getSelected(val, true)}
										/>
									</div>
									<div class="column is-half is-narrow">
										<FlightAutoSuggesstion
											title="Arrival Airport"
											icon="fas fa-plane-arrival"
											getSuggestions={this.getSuggestions}
											getSelected={(val) => this.getSelected(val, false)}
										/>
									</div>
								</div>
							</FlightItem>

							<FlightItem
								icon="fas fa-plane"
								title="What kind of Flight journey would you like to book "
								type="?"
							>
								<div className="columns">
									<div className="column is-narrow">
										<FlightCheckInput
											title="Out Going and Return "
											handleCheck={() => this.handleFlyightType(true)}
											checked={this.state.isReturn}
										/>
									</div>
									<div className="is-divider-vertical" data-content="OR" />
									<div className="column  is-narrow">
										<FlightCheckInput
											title="Out Going  only"
											handleCheck={() => this.handleFlyightType(false)}
											checked={!this.state.isReturn}
										/>
									</div>
								</div>
							</FlightItem>
							<FlightItem icon="far fa-calendar-alt" title="When would you like to Fly" type="?">
								<div className="columns is-multiline">
									<div className="column  is-narrow">
										<FlightDayPickerInput
											title="Flying Date"
											icon="far fa-calendar-alt"
											handleDayChange={(day) => this.handleDayChange(day, true)}
										/>
									</div>
									{this.state.isReturn && [
										(<div className="is-divider-vertical" data-content="OR/AND" />,
										(
											<div className="column is-narrow">
												<FlightDayPickerInput
													title="Return Date"
													icon="far fa-calendar-alt"
													handleDayChange={(day) => this.handleDayChange(day, false)}
												/>
											</div>
										))
									]}
								</div>
							</FlightItem>
							<div class="columns is-multiline is-centered">
								<div class="column is-one-quarter is-narrow">
									<p class="control">
										{this.state.isLoading && (
											<a class="button is-loading is-rounded is-medium">Loading</a>
										)}
										{this.state.isLoading || (
											<a
												class="button is-success is-rounded is-medium"
												onClick={(e) => {
													this.handleSearch(e);
												}}
											>
												<span class="icon">
													<i class="fas fa-search" />
												</span>
												<span>Show my Flights</span>
											</a>
										)}
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		);
	}
}

export default FlightFilter;
