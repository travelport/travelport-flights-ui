import React from 'react';

const FlightItem = (props) => (
	<div class="content">
		<nav class="level">
			<div class="level-left is-hidden-mobile">
				<div class="level-item">
					<p class="subtitle is-5">
						{props.title} {props.type ==='?' && <i class="fas fa-question" />}
					</p>
				</div>
			</div>
			<div class="level-right">
				<p class="buttons">
					<a class="button is-small is-primary is-focused">
						<span class="icon is-small">
							<i class={props.icon} />
						</span>
					</a>
				</p>
			</div>
		</nav>
		<div class="box">
			<article class="media">
				<div class="media-content">
					<div class="content">{props.children}</div>
				</div>
			</article>
		</div>
	</div>
);


export default FlightItem;