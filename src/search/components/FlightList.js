import React from 'react';
import FlightItem from './FlightItem';
import FlightListHeader from './FlightListHeader';
import FlightItemContents from './FlightItemContents';

const FlightList = (props) => {
	return (
		<section className="section section-feature-grey is-medium">
			<div className="container">
				<div class="columns is-centered">
					<div class="column is-three-fifths is-narrow">
						<FlightItem icon="fas fa-filter" title="Your flight preference for youre next booking ">
							<FlightListHeader filters={props.filters} />
						</FlightItem>
						<div class="columns is-multiline is-centered">
							<div class="column is-one-half is-narrow">
								<p class="control">
									<a
										class="button is-success is-rounded is-medium"
										onClick={(e) => {
											props.handleFilter();
										}}
									>
										<span class="icon">
											<i class="fas fa-search" />
										</span>
										<span>Change my Flights Search</span>
									</a>
								</p>
							</div>
						</div>
						<div class="tabs is-centered is-boxed is-medium">
							<ul>
								<li className={props.tabKey == 'G' &&"is-active"}>
									<a
									onClick={(e) => {
										props.handleTab("G");
										}}>
										<span class="icon is-small">
											<i class="fas fa-plane-departure" aria-hidden="true" />
										</span>
										<span>Out Going</span>
									</a>
								</li>
								{props.filters.isReturn &&<li className={props.tabKey == 'R' &&"is-active"}>
									<a
									onClick={(e) => {
										props.handleTab("R");
										}}>
										<span class="icon is-small">
											<i class="fas fa-plane-arrival" aria-hidden="true" />
										</span>
										<span>Return</span>
									</a>
								</li>}
								
							</ul>
						</div>
						<FlightItemContents flight={props.flights}  tabKey={props.tabKey}/>
					</div>
				</div>
			</div>
		</section>
	);
};
export default FlightList;
