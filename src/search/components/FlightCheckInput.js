import React, { Component } from 'react';

const FlightCheckInput = (props) => (
	<div className="field is-narrow">
		<div class="field has-text-primary is-uppercase has-text-weight-semibold">
			<a class="button" onClick={() => props.handleCheck()}>
				{props.checked && (
					<span class="icon">
						<i class="far fa-check-square" />
					</span>
				)}
				{props.checked || (
					<span class="icon">
						<i class="far fa-square" />
					</span>
				)}
				<span>{props.title}</span>
			</a>
		</div>
	</div>
);

export default FlightCheckInput;
