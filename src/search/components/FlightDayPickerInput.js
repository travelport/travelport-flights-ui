import React, { Component } from 'react';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';

const FlightDayPickerInput = (props) => (
	<div className="field">
		<label class="label">
			<span class="icon is-left">
				<i class={props.icon} />
			</span>
			<span className="pad-left-small">{props.title}</span>
		</label>
		<DayPickerInput onDayChange={props.handleDayChange} />
	</div>
);

export default FlightDayPickerInput;
