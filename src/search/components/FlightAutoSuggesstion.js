import React, { Component } from 'react';
import Autocomplete from 'react-autocomplete';

class FlightAutoSuggesstion extends React.Component {
	constructor() {
		super();
		this.state = {
			value: '',
			autocompleteData: []
		};
		this.onChange = this.onChange.bind(this);
		this.onSelect = this.onSelect.bind(this);
		this.getItemValue = this.getItemValue.bind(this);
		this.renderItem = this.renderItem.bind(this);
		this.retrieveDataAsynchronously = this.retrieveDataAsynchronously.bind(this);
	}

	retrieveDataAsynchronously(searchText) {
		let _this = this;

		_this.props.getSuggestions(searchText).then((res) => {
			if (res.data && res.data.length > 0) {
				_this.setState({
					autocompleteData: res.data
				});
			}
		});
	}

	onChange(e) {
		this.setState({
			value: e.target.value
		});

		this.retrieveDataAsynchronously(e.target.value);
	}

	onSelect(val) {
		this.setState({
			value: val
		});
		this.props.getSelected(val);
	}

	renderItem(item, isHighlighted) {
		return <div  style={{ background: isHighlighted ? 'lightgray' : 'white',fontSize: '18px' } }>{item.airportName}</div>;
	}

	getItemValue(item) {
		return item.airportCode;
	}

	render() {
		return (
			<div class="field">
				<label class="label">
					<span class="icon is-left">
						<i class={this.props.icon} />
					</span>
					<span className="pad-left-small">{this.props.title}</span>
				</label>
				<div class="control suggestion">
					<Autocomplete
						getItemValue={this.getItemValue}
						items={this.state.autocompleteData}
						renderItem={this.renderItem}
						value={this.state.value}
						onChange={this.onChange}
						onSelect={this.onSelect}
						wrapperStyle={{ position: 'relative', display: 'block' }}
						renderMenu={(items, value) => (
							<div className="menu">
								{value === '' ? (
									<div className="item">Type of the name of a Airport</div>
								) : this.state.loading ? (
									<div className="item">Loading...</div>
								) : items.length === 0 ? (
									<div className="item">No matches for {value}</div>
								) : (
									items
								)}
							</div>
						)}
					/>
				</div>
			</div>
		);
	}
}

export default FlightAutoSuggesstion;
