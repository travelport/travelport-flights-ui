import React from 'react';

const FlightListItem = (props) => (
	<div class="level-item">
		<div className='has-text-centered'>
			<p className="heading">{props.title}</p>
			<p className={props.subType || "title is-5"}>{props.description}</p>
		</div>
	</div>
);

export default FlightListItem;
