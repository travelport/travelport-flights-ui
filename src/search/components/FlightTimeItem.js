import React from 'react';
import FlightListItem from './FlightListItem';

const FlightTimeItem = (props) => {
	return [
		<div key={0} class="level-item ">
			<div>
				<i class={props.icon} />
			</div>
		</div>,
		<FlightListItem key={1} title={props.type} description={props.port} subType="subtitle"  />,
		<FlightListItem key={2} title="Time" description={props.time} subType="subtitle" />
	];
};

export default FlightTimeItem;
