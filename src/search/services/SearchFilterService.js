import { API } from '../../utils';

class SearchFilterService {

	getFlights = async (filter) => {
		return await API.get(`airports?q=${filter}`);
	};
}

export default SearchFilterService;
