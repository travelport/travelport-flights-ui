import { API, Log } from '../../utils';
import moment from 'moment';

class SearchListService {
	getFlights = async (filter) => {
		try {
			let flight = {
				departureFlights: [],
				returnFlights: []
			};

			let planes = await this.getPlanes();

			let promisesDepartureFlights = planes.data.map((plane) => {
				return this.searchFligths(
					plane.code,
					filter.flyingDate,
					filter.departureAirport,
					filter.destinationAirport
				);
			});
			flight.departureFlights = await Promise.all(promisesDepartureFlights);

			if (filter.isReturn) {
				let promisesReturnFlights = planes.data.map((plane) => {
					return this.searchFligths(
						plane.code,
						filter.returnDate,
						filter.destinationAirport,
						filter.departureAirport
					);
				});
				flight.returnFlights = await Promise.all(promisesReturnFlights);
			}
			return flight;
		} catch (error) {
			console.log('An error occurred.');
		}
	};

	async getPlanes() {
		try {
			return API.get('airlines');
		} catch (e) {
			Log('Error!', e);
		}
	}

	async searchFligths(code, flyingDate, departure, arrival) {
		try {
			const response = await API.get(`flight_search/${code}?date=${flyingDate}&from=${departure}&to=${arrival}`);
			return await response.data.map((flight) => {
				let searchFlight = {
					key: flight.key,
					airline: `${flight.airline.name}/${flight.airline.code}/${flight.plane
						.shortName}/${flight.flightNum}`,
					depature: flight.start.airportCode,
					deaptureTime: moment(flight.start.dateTime).format("hh:mm:ss a"),
					arrival: flight.finish.airportCode,
					arrivalTime:moment(flight.finish.dateTime).format("hh:mm:ss a"),
					duration: flight.durationMin,
					price: flight.price
				};
				return searchFlight;
			});
		} catch (e) {
			Log('Error!', e);
		}
	}
}

export default SearchListService;

