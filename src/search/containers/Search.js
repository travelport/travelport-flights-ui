import React, { Component } from 'react';
import FlightFilter from '../components/FlightFilter';
import FlightList from '../components/FlightList';
import SearchListService from '../services/SearchListService';

const searchListService = new SearchListService();

class Search extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isFilter: true,
			filters: {},
			flights: [],
			tabKey:'G'
		};
		this.handleClick = this.handleClick.bind(this);
		this.searchFlights = this.searchFlights.bind(this);
		this.reSearchFlights = this.reSearchFlights.bind(this);
		this.handleTab = this.handleTab.bind(this);
	}

	handleClick(flag) {
		this.setState(() => ({
			isFilter: flag
		}));
	}

	reSearchFlights() {
		this.handleClick(true);
	}

	handleTab(flag)
	{
		this.setState(() => ({
			tabKey: flag
		}));
	}

	async searchFlights(filters) {
		let _this = this;
		if (filters) {
			const response = await searchListService.getFlights(filters);

			const data = await response;

			_this.setState({
				filters: filters,
				flights:  data
			});
			this.handleClick(false);
		}
		else{

		}
	}

	render() {
		if (this.state.isFilter === true) {
			return <FlightFilter handleSearch={this.searchFlights} />;
		} else {
			return (
				<FlightList
					handleFilter={this.reSearchFlights}
					flights={this.state.flights}
					filters={this.state.filters}
					handleTab={this.handleTab}
					tabKey={this.state.tabKey}
				/>
			);
		}
	}
}

export default Search;
