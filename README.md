src: {
  Index.js,
  App.js: {
    Layout.js: {
      Header.js
      Content.js
      Footer.js
    }
  }
  User.js: {
    Profile.js
    PasswordReset.js
    Address.js
  }
  components: {
    SharedCompOne.js
    SharedCompTwo.js
  }
}